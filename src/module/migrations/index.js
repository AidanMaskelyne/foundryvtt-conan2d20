/* eslint-disable no-unused-vars */
import {MigrationBase} from './base';
import {Migration1MigrateTalentTree} from './1-migrate-talent-tree';
import {Migration2MigrateHealthBonus} from './2-migrate-health-bonus';

export default class Migrations {
  static list = [Migration1MigrateTalentTree, Migration2MigrateHealthBonus];

  static get latestVersion() {
    return Math.max(...this.list.map(Migration => Migration.version));
  }

  static constructAll() {
    return this.list.map(Migration => new Migration());
  }
}
