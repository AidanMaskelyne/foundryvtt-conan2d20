import {MigrationBase} from './base';

export class Migration2MigrateHealthBonus extends MigrationBase {
  static version = 0.220106;

  async updateActor(actor) {
    if (!actor.data.health.physical.bonus || !actor.data.health.mental.bonus) {
      console.log(
        'Found Actor missing health bonus from previous schema. Migrating actor schema.'
      );
      actor.data.health.physical.bonus = 0;
      actor.data.health.mental.bonus = 0;
    }
  }
}
