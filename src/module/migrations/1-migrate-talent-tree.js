import {MigrationBase} from './base';

export class Migration1MigrateTalentTree extends MigrationBase {
  static version = 0.031421;

  async updateItem(item) {
    if (item.type === 'talent' && item.data.skill) {
      console.log(
        'Found Talent item from previous schema. Migrating item schema.'
      );
      if (!item.data.tree) {
        item.data.tree =
          game.i18n.localize(CONFIG.skills[item.data.skill]) || '';
      }
      console.log('Removing unused field in item schema.');
      delete item.data.skill;
    }
  }
}
