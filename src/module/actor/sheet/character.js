/* eslint-disable no-undef */

import ActorSheetConan2d20 from './base';
import C2Utility from '../../../scripts/utility';
import {calculateEncumbrance} from '../../item/encumbrance';

class ActorSheetConan2d20Character extends ActorSheetConan2d20 {
  static get defaultOptions() {
    const options = super.defaultOptions;
    mergeObject(options, {
      classes: options.classes.concat([
        'conan2d20',
        'actor',
        'pc',
        'character-sheet',
      ]),
      width: 650,
      height: 800,
      tabs: [
        {
          navSelector: '.sheet-navigation',
          contentSelector: '.sheet-content',
          initial: 'character',
        },
      ],
      dragDrop: [{dragSelector: '.item-list .item', dropSelector: null}],
    });
    return options;
  }

  get template() {
    const path = 'systems/conan2d20/templates/actors/';
    if (!game.user.isGM && this.actor.limited)
      return `${path}readonly-sheet.html`;
    return `${path}character-sheet.html`;
  }

  getData() {
    const sheetData = super.getData();
    // Update skill labels
    if (sheetData.data.data.skills !== undefined) {
      for (const [s, skl] of Object.entries(sheetData.data.data.skills)) {
        skl.label = CONFIG.CONAN.skills[s];
      }
    }

    // Update Encumbrance Level
    sheetData.data.data.encumbrance = calculateEncumbrance(
      sheetData.actor.inventory,
      sheetData.actor.data.data.attributes.bra.value
    );

    // Update wounded icon
    sheetData.data.data.health.physical.wounds = C2Utility.addDots(
      duplicate(sheetData.data.data.health.physical.wounds),
      sheetData.data.data.health.physical.wounds.max
    );

    // Update traumatized icon
    sheetData.data.data.health.mental.traumas = C2Utility.addDots(
      duplicate(sheetData.data.data.health.mental.traumas),
      sheetData.data.data.health.mental.traumas.max
    );

    // Update Actor Gold
    sheetData.data.data.gold = sheetData.actor.data.data.resources.gold.value;

    // Update Actor Armor values
    if (
      sheetData.actor.inventory.weapon.items.filter(
        i => i.data.data.group === 'shield'
      ).length > 0
    ) {
      const shields = sheetData.actor.inventory.weapon.items.filter(
        i => i.data.data.group === 'shield'
      );
      sheetData.data.armor = C2Utility.calculateArmor(
        sheetData.actor.inventory.armor.items,
        shields
      );
    } else {
      sheetData.data.armor = C2Utility.calculateArmor(
        sheetData.actor.inventory.armor.items,
        undefined
      );
    }

    sheetData.skills = CONFIG.CONAN.skills;

    return sheetData;
  }

  getMaxResolve(actorData) {
    return (
      actorData.data.attributes.wil.value +
      actorData.data.skills.dis.expertise.value -
      actorData.data.health.mental.despair +
      actorData.data.health.mental.bonus
    );
  }

  getMaxVigor(actorData) {
    return (
      actorData.data.attributes.bra.value +
      actorData.data.skills.res.expertise.value -
      actorData.data.health.physical.fatigue +
      actorData.data.health.physical.bonus
    );
  }

  /* -------------------------------------------- */

  /**
   * Organize and classify Items for Character sheets
   * @private
   */

  _prepareItems(actorData) {
    const inventory = {
      armor: {
        label: game.i18n.localize('CONAN.inventoryArmorHeader'),
        items: [],
      },
      weapon: {
        label: game.i18n.localize('CONAN.inventoryWeaponsHeader'),
        items: [],
      },
      kit: {label: game.i18n.localize('CONAN.inventoryKitsHeader'), items: []},
      consumable: {
        label: game.i18n.localize('CONAN.inventoryConsumablesHeader'),
        items: [],
      },
      transportation: {
        label: game.i18n.localize('CONAN.transpoHeader'),
        items: [],
      },
      miscellaneous: {
        label: game.i18n.localize('CONAN.inventoryMiscHeader'),
        items: [],
      },
    };

    const talents = [];

    const sorcery = {
      enchantment: {
        label: game.i18n.localize('CONAN.sorceryEnchantmentHeader'),
        sorcery: [],
      },
      spell: {
        label: game.i18n.localize('CONAN.sorcerySpellHeader'),
        sorcery: [],
      },
    };

    // Actions
    const actions = {
      standard: {
        label: game.i18n.localize('CONAN.actionsStandardActionHeader'),
        actions: [],
      },
      minor: {
        label: game.i18n.localize('CONAN.actionsMinorActionHeader'),
        actions: [],
      },
      reaction: {
        label: game.i18n.localize('CONAN.actionsReactionsHeader'),
        actions: [],
      },
      free: {
        label: game.i18n.localize('CONAN.actionsFreeActionsHeader'),
        actions: [],
      },
    };

    // Read-Only Actions
    const readonlyActions = {
      interaction: {label: 'Interaction Actions', actions: []},
      defensive: {label: 'Defensive Actions', actions: []},
      offensive: {label: 'Offensive Actions', actions: []},
    };

    const readonlyEquipment = [];

    const attacks = {
      display: [],
      weapon: [],
    };

    // Pre-sort all items so that when they are filtered into their relevant
    // categories they are already sorted alphabetically (case-sensitive)
    const allItems = [];
    (actorData.items ?? []).forEach(item => allItems.push(item));

    allItems.sort((a, b) => {
      if (a.name < b.name) {
        return -1;
      }
      if (a.name > b.name) {
        return 1;
      }
      return 0;
    });

    let talentTrees = {};

    for (const i of allItems) {
      i.data.img = i.data.img || DEFAULT_TOKEN;

      // Read-Only Equipment
      if (i.type === 'armor' || i.type === 'consumable' || i.type === 'kit') {
        readonlyEquipment.push(i);
        actorData.hasEquipment = true;
      }

      if (i.type === 'armor' || i.type === 'weapon') {
        i.canBeEquipped = true;
        i.canBeBroken = true;
      } else {
        i.canBeEquipped = false;
        i.canBeBroken = false;
      }

      if (i.data.data.equipped) {
        i.isEquipped = true;
      } else {
        i.isEquipped = false;
      }

      // Filter all displays and equipped weapons into the relevant attack
      // arrays for display on the actions tab
      if (i.type === 'display' || i.type === 'weapon') {
        const action = {};
        action.imageUrl = i.img;
        action.name = i.name;
        action.type = 'attack';
        const flavor = C2Utility.getAttackDescription(i);
        action.description = flavor.description;
        action.success = flavor.success;

        action.qualities = [
          {
            name: 'attack',
            label: game.i18n.localize(CONFIG.attacks[i.type]),
          },
        ];

        if (i.type === 'weapon') {
          action.qualities.push(
            {
              name: 'weaponType',
              label: CONFIG.weaponTypes[i.data.weaponType],
            },
            {
              name: 'weapongroup',
              label: CONFIG.weaponGroups[i.data.group] ?? '',
            }
          );
        }

        i?.data?.data?.qualities?.value?.map(quality => {
          const key = CONFIG.weaponQualities[quality] ?? quality;

          let qualityLabel = '';
          if (key.value) {
            qualityLabel = `${game.i18n.localize(key.label)}(${key.value})`;
          } else {
            qualityLabel = `${game.i18n.localize(key.label)}`;
          }

          action.qualities.push({
            name: quality,
            label: qualityLabel,
            description: CONFIG.qualitiesDescriptions[key.type] || '',
          });
        });

        action.attack = {};
        action.attack.id = i.id;
        action.attack.type = i.type;

        if (i.type === 'display' || i.isEquipped) {
          attacks[i.type].push(action);
        }
      }

      // Inventory
      if (Object.keys(inventory).includes(i.type)) {
        i.data.data.quantity = i.data.data.quantity || 0;
        i.data.data.encumbrance = i.data.data.encumbrance || 0;
        i.hasCharges = i.type === 'kit' && i.data.data.uses.max > 0;

        inventory[i.type].items.push(i);
      } else if (i.type === 'talent') {
        const itemsTalentTree = i.data.data.tree.toLowerCase() || 'other';

        if (!talentTrees[itemsTalentTree]) {
          talentTrees[itemsTalentTree] = {
            label: itemsTalentTree,
            ranks: 0,
            talentCount: 0,
            talents: [],
          };
        }

        talentTrees[itemsTalentTree].talents.push(i);
        talentTrees[itemsTalentTree].ranks += i.data.data.rank.value;
        talentTrees[itemsTalentTree].talentCount += 1;

        const actionType = i.data.data.actionType || 'passive';

        if (Object.keys(actions).includes(actionType)) {
          i.talent = true;
          actions[actionType].actions.push(i);

          // Read-Only Actions
          if (i.data.actionCategory && i.data.actionCategory) {
            switch (i.data.actionCategory) {
              case 'interaction':
                readonlyActions.interaction.actions.push(i);
                actorData.hasInteractionActions = true;
                break;
              case 'defensive':
                readonlyActions.defensive.actions.push(i);
                actorData.hasDefensiveActions = true;
                break;
              // Should be offensive but throw anything else in there too
              default:
                readonlyActions.offensive.actions.push(i);
                actorData.hasOffensiveActions = true;
            }
          } else {
            readonlyActions.offensive.actions.push(i);
            actorData.hasOffensiveActions = true;
          }
        }
      } else if (i.type === 'spell') {
        sorcery[i.type].sorcery.push(i);
      } else if (i.type === 'enchantment') {
        sorcery[i.type].sorcery.push(i);
        if (i.data.data.uses.value > 0) {
          inventory.consumable.items.push(i);
        }
      } else if (i.type === 'transportation') {
        inventory.transportation.items.push(i);
      } else if (i.type === 'miscellaneous') {
        inventory.miscellaneous.items.push(i);
      }

      // Invalid Items
      if (i.type === 'npcaction') {
        alert('NPC Action is not a valid item for player characters');
        this.actor.deleteEmbeddedDocuments('Item', [i.id]);
      } else if (i.type === 'npcattack') {
        alert('NPC Attack is not a valid item for player characters.');
        this.actor.deleteEmbeddedDocuments('Item', [i.id]);
      }

      // Actions
      if (i.type === 'action') {
        const actionType = i.data.data.actionType || 'action';
        if (actionType === 'passive') {
          actions.free.actions.push(i);
        } else {
          actions[actionType].actions.push(i);

          // Read-Only Actions
          if (i.data.actionCategory) {
            switch (i.data.actionCategory) {
              case 'interaction':
                readonlyActions.interaction.actions.push(i);
                actorData.hasInteractionActions = true;
                break;
              case 'defensive':
                readonlyActions.defensive.actions.push(i);
                actorData.hasDefensiveActions = true;
                break;
              case 'offensive':
                // if (i)
                readonlyActions.offensive.actions.push(i);
                actorData.hasOffensiveActions = true;
                break;
              // Should be offensive but throw anything else in there too
              default:
                readonlyActions.offensive.actions.push(i);
                actorData.hasOffensiveActions = true;
            }
          } else {
            readonlyActions.offensive.actions.push(i);
            actorData.hasOffensiveActions = true;
          }
        }
      }
    }

    // Sort the discovered talent trees and add them to the talents data to
    // be stored in the actorData
    Object.keys(talentTrees)
      .sort()
      .forEach(talent => {
        talents.push(talentTrees[talent]);
      });

    // Assign and return
    actorData.actions = actions;
    actorData.attacks = attacks;
    actorData.inventory = inventory;
    actorData.readonlyactions = readonlyActions;
    actorData.readonlyEquipment = readonlyEquipment;
    actorData.sorcery = sorcery;
    actorData.talents = talents;
  }

  _adjustDespair(actorData, delta) {
    let currentMax = this.getMaxResolve(actorData);

    actorData.data.health.mental.despair += delta;
    currentMax -= delta;

    // clamp values if out of range
    if (actorData.data.health.mental.despair < 0) {
      actorData.data.health.mental.despair = 0;
    } else if (actorData.data.health.mental.value > currentMax) {
      actorData.data.health.mental.value = currentMax;
      actorData.data.health.mental.max = currentMax;
    }

    game.actors.get(actorData._id).update(actorData);
  }

  _adjustFatigue(actorData, delta) {
    let currentMax = this.getMaxVigor(actorData);

    actorData.data.health.physical.fatigue += delta;
    currentMax -= delta;

    // clamp values if out of range
    if (actorData.data.health.physical.fatigue < 0) {
      actorData.data.health.physical.fatigue = 0;
    } else if (actorData.data.health.physical.value > currentMax) {
      actorData.data.health.physical.value = currentMax;
      actorData.data.health.physical.max = currentMax;
    }

    game.actors.get(actorData._id).update(actorData);
  }

  _adjustResolve(actorData, delta) {
    const currentMax = this.getMaxResolve(actorData);

    actorData.data.health.mental.value += delta;

    // clamp values if out of range
    if (actorData.data.health.mental.value < 0) {
      actorData.data.health.mental.value = 0;
    } else if (actorData.data.health.mental.value > currentMax) {
      actorData.data.health.mental.value = currentMax;
    }

    game.actors.get(actorData._id).update(actorData);
  }

  _adjustResolveBonus(actorData, delta) {
    const currentMax = this.getMaxResolve(actorData);

    // don't add a negative delta if the max value is already 0
    if (!(currentMax === 0 && delta < 0)) {
      actorData.data.health.mental.bonus += delta;

      // also apply the bonus immediately to the current health value
      actorData.data.health.mental.value += delta;
    }

    game.actors.get(actorData._id).update(actorData);
  }

  _adjustVigor(actorData, delta) {
    const currentMax = this.getMaxVigor(actorData);

    actorData.data.health.physical.value += delta;

    // clamp values if out of range
    if (actorData.data.health.physical.value < 0) {
      actorData.data.health.physical.value = 0;
    } else if (actorData.data.health.physical.value > currentMax) {
      actorData.data.health.physical.value = currentMax;
    }

    game.actors.get(actorData._id).update(actorData);
  }

  _adjustVigorBonus(actorData, delta) {
    const currentMax = this.getMaxVigor(actorData);

    // don't add a negative delta if the max value is already 0
    if (!(currentMax === 0 && delta < 0)) {
      actorData.data.health.physical.bonus += delta;

      // also apply the bonus immediately to the current health value
      actorData.data.health.physical.value += delta;
    }

    game.actors.get(actorData._id).update(actorData);
  }

  _resetDespair(actorData) {
    actorData.data.health.mental.despair = 0;

    game.actors.get(actorData._id).update(actorData);
  }

  _resetFatigue(actorData) {
    actorData.data.health.physical.fatigue = 0;

    game.actors.get(actorData._id).update(actorData);
  }

  _resetResolve(actorData) {
    const currentMax = this.getMaxResolve(actorData);

    actorData.data.health.mental.value = currentMax;

    game.actors.get(actorData._id).update(actorData);
  }

  _resetResolveBonus(actorData) {
    actorData.data.health.mental.bonus = 0;

    const currentMax = this.getMaxResolve(actorData);

    // clamp the current value to the max if it is higher than the new maximum
    if (actorData.data.health.mental.value > currentMax) {
      actorData.data.health.mental.value = currentMax;
    }

    game.actors.get(actorData._id).update(actorData);
  }

  _resetVigor(actorData) {
    const currentMax = this.getMaxVigor(actorData);

    actorData.data.health.physical.value = currentMax;

    game.actors.get(actorData._id).update(actorData);
  }

  _resetVigorBonus(actorData) {
    actorData.data.health.physical.bonus = 0;

    const currentMax = this.getMaxVigor(actorData);

    // clamp the current value to the max if it is higher than the new maximum
    if (actorData.data.health.physical.value > currentMax) {
      actorData.data.health.physical.value = currentMax;
    }

    game.actors.get(actorData._id).update(actorData);
  }

  activateListeners(html) {
    super.activateListeners(html);

    if (!this.options.editable) return;

    html.find('img[data-edit]').click(ev => this._onEditImage(ev));

    html.find('.condition-value').mouseup(ev => {
      const condKey = $(ev.currentTarget)
        .parents('.sheet-condition')
        .attr('data-cond-id');
      if (ev.button === 0) {
        this.actor.addCondition(condKey);
      } else if (ev.button === 2) {
        this.actor.removeCondition(condKey);
      }
    });

    // Despair mouse controls
    html.find('.despair-tracker').mouseup(ev => {
      const actorData = duplicate(this.actor);

      if (ev.button === 0) {
        if (window.event.ctrlKey) {
          this._resetDespair(actorData);
        } else {
          this._adjustDespair(actorData, 1);
        }
      } else if (ev.button === 2) {
        this._adjustDespair(actorData, -1);
      }
    });

    html.find('.despair-tracker').on('wheel', ev => {
      const actorData = duplicate(this.actor);
      if (ev.originalEvent.deltaY < 0) {
        this._adjustDespair(actorData, 1);
      } else if (ev.originalEvent.deltaY > 0) {
        this._adjustDespair(actorData, -1);
      }
    });

    // Fatigue mouse controls
    html.find('.fatigue-tracker').mouseup(ev => {
      const actorData = duplicate(this.actor);

      if (ev.button === 0) {
        if (window.event.ctrlKey) {
          this._resetFatigue(actorData);
        } else {
          this._adjustFatigue(actorData, 1);
        }
      } else if (ev.button === 2) {
        this._adjustFatigue(actorData, -1);
      }
    });

    html.find('.fatigue-tracker').on('wheel', ev => {
      const actorData = duplicate(this.actor);
      if (ev.originalEvent.deltaY < 0) {
        this._adjustFatigue(actorData, 1);
      } else if (ev.originalEvent.deltaY > 0) {
        this._adjustFatigue(actorData, -1);
      }
    });

    // Resolve Bonus mouse controls
    html.find('.resolve-max').mouseup(ev => {
      const actorData = duplicate(this.actor);
      if (ev.button === 0) {
        if (window.event.ctrlKey) {
          this._resetResolveBonus(actorData);
        } else {
          this._adjustResolveBonus(actorData, 1);
        }
      } else if (ev.button === 2) {
        this._adjustResolveBonus(actorData, -1);
      }
    });

    html.find('.resolve-max').on('wheel', ev => {
      const actorData = duplicate(this.actor);
      if (ev.originalEvent.deltaY < 0) {
        this._adjustResolveBonus(actorData, 1);
      } else if (ev.originalEvent.deltaY > 0) {
        this._adjustResolveBonus(actorData, -1);
      }
    });

    // Resolve mouse controls
    html.find('.resolve-value').mouseup(ev => {
      const actorData = duplicate(this.actor);

      if (ev.button === 0) {
        if (window.event.ctrlKey) {
          this._resetResolve(actorData);
        } else {
          this._adjustResolve(actorData, 1);
        }
      } else if (ev.button === 2) {
        this._adjustResolve(actorData, -1);
      }
    });

    html.find('.resolve-value').on('wheel', ev => {
      const actorData = duplicate(this.actor);
      if (ev.originalEvent.deltaY < 0) {
        this._adjustResolve(actorData, 1);
      } else if (ev.originalEvent.deltaY > 0) {
        this._adjustResolve(actorData, -1);
      }
    });

    // Vigor Bonus mouse controls
    html.find('.vigor-max').on('wheel', ev => {
      const actorData = duplicate(this.actor);
      if (ev.originalEvent.deltaY < 0) {
        this._adjustVigorBonus(actorData, 1);
      } else if (ev.originalEvent.deltaY > 0) {
        this._adjustVigorBonus(actorData, -1);
      }
    });

    html.find('.vigor-max').mouseup(ev => {
      const actorData = duplicate(this.actor);
      if (ev.button === 0) {
        if (window.event.ctrlKey) {
          this._resetVigorBonus(actorData);
        } else {
          this._adjustVigorBonus(actorData, 1);
        }
      } else if (ev.button === 2) {
        this._adjustVigorBonus(actorData, -1);
      }
    });

    // Vigor mouse controls
    html.find('.vigor-value').mouseup(ev => {
      const actorData = duplicate(this.actor);

      if (ev.button === 0) {
        if (window.event.ctrlKey) {
          this._resetVigor(actorData);
        } else {
          this._adjustVigor(actorData, 1);
        }
      } else if (ev.button === 2) {
        this._adjustVigor(actorData, -1);
      }
    });

    html.find('.vigor-value').on('wheel', ev => {
      const actorData = duplicate(this.actor);
      if (ev.originalEvent.deltaY < 0) {
        this._adjustVigor(actorData, 1);
      } else if (ev.originalEvent.deltaY > 0) {
        this._adjustVigor(actorData, -1);
      }
    });

    html.find('.condition-toggle').mouseup(ev => {
      const condKey = $(ev.currentTarget)
        .parents('.sheet-condition')
        .attr('data-cond-id');

      if (
        game.conan2d20.config.statusEffects.find(e => e.id === condKey).flags
          .conan2d20.value === null
      ) {
        if (this.actor.hasCondition(condKey)) {
          this.actor.removeCondition(condKey);
        } else {
          this.actor.addCondition(condKey);
        }
        return;
      }

      if (ev.button === 0) {
        this.actor.addCondition(condKey);
      } else if (ev.button === 2) {
        this.actor.removeCondition(condKey);
      }
    });
  }
}

export default ActorSheetConan2d20Character;
