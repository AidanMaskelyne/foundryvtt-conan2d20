/* eslint-disable no-unused-vars, no-shadow */

/**
 * Extend the base Actor class to implement additiona logic specialized for Conan2d20
 */
import {CONFIG} from '../../scripts/config';
import Conan2d20Dice from '../system/rolls';
import Counter from '../system/counter';
import CharacterData from './character';
import C2Utility from '../../scripts/utility';

export default class Conan2d20Actor extends Actor {
  /**
   *
   * Set initical actor data based on type
   *
   */
  async _preCreate(data, options, user) {
    await super._preCreate(data, options, user);
    const nameMode = game.settings.get('conan2d20', 'defaultTokenSettingsName');
    const barMode = game.settings.get('conan2d20', 'defaultTokenSettingsBar');

    const createData = {};

    if (!data.token) {
      mergeObject(createData, {
        'token.bar1': {attribute: 'data.data.health.physical'}, // Default Bar 1 to Wounds
        'token.bar2': {attribute: 'data.data.health.mental'}, // Default Bar 1 to Wounds
        'token.displayName': nameMode, // Default display name to be on owner hover
        'token.displayBars': barMode, // Default display bars to be on owner hover
        'token.disposition': CONST.TOKEN_DISPOSITIONS.HOSTILE, // Default disposition to hostile
        'token.name': data.name, // Set token name to actor name
      });
    } else if (data.token) {
      createData.token = data.token;
    }

    if (data.type === 'character') {
      createData.token.vision = true;
      createData.token.actorLink = true;
    }

    if (data.type === 'npc') {
      createData.token.actorLink = true;
    }

    this.data.update(createData);
  }

  /**
   * Augment the basic actor data with additional dynamic data
   */
  prepareData() {
    super.prepareData();

    // Get the Actor's data object
    const actorData = this.data;
    const {data} = actorData;

    // Prepare Character data
    if (actorData.type === 'character') this._prepareCharacterData(actorData);
    else if (actorData.type === 'npc') this._prepareNPCData(actorData);

    if (data.qualities !== undefined) {
      const map = {};
      for (const [t] of Object.entries(map)) {
        const quality = data.qualities[t];
        if (quality === undefined) {
          /* eslint-disable-next-line no-continue */
          continue;
        }
      }
    }

    // Return the prepared Actor data
    return actorData;
  }

  /* -------------------------------------------- */

  /**
   * Prepare Character type specific data
   */
  _prepareCharacterData(actorData) {
    const {data} = actorData;
    const character = new CharacterData(data, this.items);

    // Calculate Vigor
    if (
      isNaN(data.health.physical.bonus) ||
      data.health.physical.bonus === null
    ) {
      data.health.physical.bonus = 0;
    }

    data.health.physical.max =
      data.attributes.bra.value +
      data.skills.res.expertise.value -
      data.health.physical.fatigue +
      data.health.physical.bonus;

    if (data.health.physical.value === null) {
      data.health.physical.value =
        data.attributes.bra.value + data.skills.res.expertise.value;
    } else if (data.health.physical.value > data.health.physical.max) {
      data.health.physical.value = data.health.physical.max;
    } else if (data.health.physical.value < 0) {
      data.health.physical.value = 0;
    }

    // Calculate Resolve
    if (isNaN(data.health.mental.bonus) || data.health.mental.bonus === null) {
      data.health.mental.bonus = 0;
    }

    data.health.mental.max =
      data.attributes.wil.value +
      data.skills.dis.expertise.value -
      data.health.mental.despair +
      data.health.mental.bonus;

    if (data.health.mental.value === null) {
      data.health.mental.value =
        data.attributes.wil.value + data.skills.dis.expertise.value;
    } else if (data.health.mental.value > data.health.mental.max) {
      data.health.mental.value = data.health.mental.max;
    } else if (data.health.mental.value < 0) {
      data.health.mental.value = 0;
    }

    // Set TN for Skills
    for (const [s, skl] of Object.entries(data.skills)) {
      skl.tn.value = skl.expertise.value + data.attributes[skl.attribute].value;
      if (data.skills[s].expertise.value > 0) {
        data.skills[s].trained = true;
      }
    }

    // Prepare Upkeep Cost
    data.resources.upkeep.value =
      3 + data.background.standing.value - data.background.renown;
    if (data.resources.upkeep.value < 0) {
      data.resources.upkeep.value = 0;
    }

    // Automatic Actions
    data.actions = [];

    // Experience
    data.resources.xp.value = character.exp;
  }

  /* -------------------------------------------- */

  /**
   * Prepare Character type specific data
   */
  _prepareNPCData(actorData) {
    const {data} = actorData;
    const npc = new CharacterData(data, this.items);
    return npc;
  }

  static addDoom(doomSpend) {
    Counter.changeCounter(+`${doomSpend}`, 'doom');
  }

  static addMomentum(momentumSpend) {
    Counter.changeCounter(+`${momentumSpend}`, 'momentum');
  }

  static spendFortune(actorData, fortuneSpend) {
    const newValue = actorData.data.resources.fortune.value - fortuneSpend;
    if (newValue < 0) {
      const error = 'Fortune spend would exceed available fortune points.';
      throw error;
    } else {
      actorData.data.resources.fortune.value = newValue;
      game.actors.get(actorData._id).update(actorData);
    }
  }

  static spendDoom(doomSpend) {
    const newValue = game.settings.get('conan2d20', 'doom') - doomSpend;
    if (newValue < 0) {
      const error = 'Doom spend would exceed available doom points.';
      throw error;
    } else {
      Counter.changeCounter(-`${doomSpend}`, 'doom');
    }
  }

  static spendMomentum(momentumSpend) {
    const newValue = game.settings.get('conan2d20', 'momentum') - momentumSpend;
    if (newValue < 0) {
      const error = 'Momentum spend would exceed available momentum points.';
      throw error;
    } else {
      Counter.changeCounter(-`${momentumSpend}`, 'momentum');
    }
  }

  static spendReload(actorData, reloadSpend, reloadItem) {
    const newValue =
      actorData.items.find(i => i._id === reloadItem).data.uses.value -
      reloadSpend;
    if (newValue < 0) {
      const error = 'Resource spend would exceed available reloads.';
      throw error;
    } else {
      actorData.items.find(
        i => i._id === reloadItem
      ).data.uses.value = newValue;
      game.actors.get(actorData._id).update(actorData);
    }
  }

  triggerReroll(message, type) {
    const msgdata = message.data.flags.data;
    const rerolls = [];
    $(message.data.content)
      .children('.roll.selected')
      .each(function () {
        rerolls.push(this.innerHTML.trim());
      });

    const norolls = [];
    $(message.data.content)
      .children('.roll:not(.selected)')
      .each(function () {
        norolls.push(this.innerHTML.trim());
      });

    const diceQty = rerolls.length;

    let html = `<h3 class="center"><b>${game.i18n.localize(
      'CONAN.skillRerollActivate'
    )}</b></h3>`;
    if (type === 'skill') {
      /* eslint-disable-next-line prefer-template */
      html += `${game.i18n.format('CONAN.skillRerollText', {
        character: `<b>${this.name}</b>`,
      })}<br>`;
    } else if (type === 'damage') {
      /* eslint-disable-next-line prefer-template */
      html += `${game.i18n.format('CONAN.damageRerollText', {
        character: `<b>${this.name}</b>`,
      })}<br>`;
    }

    const chatData = {
      user: game.user.id,
      rollMode: 'reroll',
      content: html,
    };

    ChatMessage.create(chatData);

    const cardData = {
      title: `${msgdata.title} Re-Roll`,
      speaker: {
        alias: message.data.speaker.alias,
        actor: message.data.speaker.actor,
      },
      template: msgdata.template,
      flags: {
        img: this.data.token.randomImg ? this.data.img : this.data.token.img,
      },
    };

    if (type === 'skill') {
      Conan2d20Dice.calculateSkillRoll(
        diceQty,
        msgdata.rollData.tn,
        msgdata.rollData.focus,
        msgdata.rollData.trained,
        msgdata.resultData.difficulty,
        undefined,
        cardData,
        norolls
      );
    } else if (type === 'damage') {
      Conan2d20Dice.calculateDamageRoll(
        diceQty,
        msgdata.resultData.damageType,
        cardData,
        undefined,
        norolls
      );
    }
  }

  /**
   *
   * @param message: Message content from chat card
   * @param type
   */
  momentumSpendImmediate(message, type) {
    const generated = message.data.flags.data.resultData.momentumGenerated;
    Conan2d20Dice.showRollMomentumSpendDialog(generated, message);
  }

  /**
   * Setup a Skill Test.
   *
   * Skill tests are fairly simple but there are a number of validations that
   * need to be made including the handling of fortune and doom/momentum
   */
  setupSkill(skill, actorType) {
    let skillList;
    let attrList;
    if (actorType === 'character') {
      skillList = CONFIG.skills;
      attrList = CONFIG.attributes;
    } else if (actorType === 'npc') {
      skillList = CONFIG.expertiseFields;
    }
    if (skillList[skill]) {
      const dialogData = {
        title: skillList[skill],
        modifiers: this._getModifiers('skill', {skill, actorType}),
        template: 'systems/conan2d20/templates/apps/skill-roll-dialogue.html',
      };
      const rollData = {
        skill: this.data.data.skills[skill],
      };
      const cardData = this._setupCardData(
        'systems/conan2d20/templates/chat/skill-roll-card.html',
        skillList[skill]
      );
      return {dialogData, cardData, rollData};
    } else if (attrList[skill]) {
      const dialogData = {
        title: attrList[skill],
        modifiers: this._getModifiers('skill', {skill, actorType}),
        template: 'systems/conan2d20/templates/apps/skill-roll-dialogue.html',
      };
      const rollData = {
        skill: this.data.data.attributes[skill],
        attrRoll: true,
      };
      const cardData = this._setupCardData(
        'systems/conan2d20/templates/chat/skill-roll-card.html',
        attrList[skill]
      );
      return {dialogData, cardData, rollData};
    }
  }

  /**
   * Setup a Weapons Test.
   *
   * Probably the most complex test in the game.
   */
  setupWeapon(weapon, options = []) {
    const title = `${game.i18n.localize('Damage Roll')} - ${weapon.name}`;
    const dialogData = {
      title,
      modifiers: this._getModifiers('damage', weapon),
      template: 'systems/conan2d20/templates/apps/damage-roll-dialogue.html',
    };
    if (options.length) {
      dialogData.options = options;
    }
    const cardData = this._setupCardData(
      'systems/conan2d20/templates/chat/damage-roll-card.html',
      title
    );

    const rollData = {
      target: 0,
      hitLocation: true,
      extra: {
        weapon,
      },
    };
    return {dialogData, cardData, rollData};
  }

  _setupCardData(template, title) {
    const cardData = {
      title: `${title} Test`,
      speaker: {
        alias: this.data.token.name,
        actor: this.data._id,
      },
      template,
      flags: {
        img: this.data.token.randomImg ? this.data.img : this.data.token.img,
      },
    };

    if (this.token) {
      cardData.speaker.alias = this.token.data.name;
      cardData.speaker.token = this.token.data.id;
      cardData.speaker.scene = canvas.scene.id;
      cardData.flags.img = this.token.data.img;
    } else {
      const speaker = ChatMessage.getSpeaker();
      if (speaker.actor === this.data.id) {
        cardData.speaker.alias = speaker.alias;
        cardData.speaker.token = speaker.token;
        cardData.speaker.scene = speaker.scene;
        cardData.flags.img = speaker.token
          ? canvas.tokens.get(speaker.token).data.img
          : cardData.flags.img;
      }
    }
    return cardData;
  }

  _getModifiers(type, specifier) {
    let mod;
    if (type === 'skill') {
      const difficultyLevels = CONFIG.rollDifficultyLevels;
      const diceModSpends = CONFIG.skillRollResourceSpends;
      const prefilledDifficulty = this._getPrefilledDifficulty(specifier.skill);
      const prefilledAttribute = this._getPrefilledAttribute(specifier.skill);
      if (specifier.actorType === 'npc') {
        mod = {
          difficulty: difficultyLevels,
          prefilledDifficulty: prefilledDifficulty.difficulty,
          prefilledAttribute: prefilledAttribute.attribute,
          diceModifier: diceModSpends,
          successModifier: 0,
          npcAttributes: CONFIG.attributes,
          actorType: specifier.actorType,
        };
        return mod;
      }
      mod = {
        difficulty: difficultyLevels,
        prefilledDifficulty: prefilledDifficulty.difficulty,
        difficultyTooltip: prefilledDifficulty.tooltipText,
        diceModifier: diceModSpends,
        successModifier: 0,
        actorType: specifier.actorType,
      };
      return mod;
    }
    if (type === 'damage') {
      const attackTypes = CONFIG.weaponTypes;
      let wType = '';
      let attacker = '';
      const bDamage = {};
      let attributeBonus = 0;
      if (specifier.type === 'display') {
        wType = 'display';
        attributeBonus = this._attributeBonus('per');
        attacker = 'character';
      } else if (specifier.type === 'weapon') {
        wType = specifier.data.weaponType;
        if (wType === 'melee') attributeBonus = this._attributeBonus('bra');
        else if (wType === 'ranged')
          attributeBonus = this._attributeBonus('awa');
        attacker = 'character';
      } else if (specifier.type === 'npcattack') {
        attacker = 'npc';
      }
      mod = {
        attacker,
        attackType: attackTypes,
        weaponType: wType,
        momentumModifier: 0,
        reloadModifier: 0,
        talentModifier: 0,
        attributeBonus,
      };
      if (specifier.data.damage.dice === 'x') {
        mod = mergeObject(mod, {baseDamage: specifier.data.damage.dice});
      }
    }
    return mod;
  }

  /**
   * Uses selected field of expertise to determine which attribute the test should use by default.
   * @param expertise Expertise being rolled
   */
  _getPrefilledAttribute(expertise) {
    return {attribute: CONFIG.expertiseAttributeMap[expertise]};
  }

  /**
   * Uses contextual data like conditions or harms to determine how much to increase test difficulty by
   * @param skill Skill being used
   */
  _getPrefilledDifficulty(skill) {
    const tooltip = [];
    let difficulty = 1;
    if (this.hasCondition('dazed')) {
      difficulty += 1;
      tooltip.push({name: CONFIG.conditionTypes.dazed, value: 1});
    }

    // Open to change here, as what is affected and what isn't is up in the air
    const blindedAffected = [
      'obs',
      'ins',
      'ran',
      'mel',
      'sai',
      'par',
      'cmb',
      'mov',
      'sns',
    ];
    if (this.hasCondition('blind') && blindedAffected.includes(skill)) {
      difficulty += 2;
      tooltip.push({name: CONFIG.conditionTypes.blind, value: 2});
    }

    const deafAffected = ['obs', 'ins', 'com', 'per', 'sns'];
    if (this.hasCondition('deaf') && deafAffected.includes(skill)) {
      difficulty += 2;
      tooltip.push({name: CONFIG.conditionTypes.deaf, value: 2});
    }

    if (this.actorType === 'character') {
      const physicalTests = ['agi', 'bra', 'coo'];
      const mentalTests = ['awa', 'int', 'per', 'wil'];

      const wounds = Object.values(
        this.data.data.health.physical.wounds.dots
      ).reduce((acc, w) => {
        acc += w.status === 'wounded' ? 1 : 0;
        return acc;
      }, 0);

      if (
        wounds > 0 &&
        physicalTests.includes(CONFIG.skillAttributeMap[skill])
      ) {
        difficulty += wounds;
        tooltip.push({name: 'Wounds', value: wounds});
      }

      const trauma = Object.values(
        this.data.data.health.mental.traumas.dots
      ).reduce((acc, w) => {
        acc += w.status === 'wounded' ? 1 : 0;
        return acc;
      }, 0);
      if (trauma > 0 && mentalTests.includes(CONFIG.skillAttributeMap[skill])) {
        difficulty += trauma;
        tooltip.push({name: 'Trauma', value: trauma});
      }
    } else if (this.actorType === 'npc') {
      const physicalTests = ['agi', 'bra'];
      const mentalTests = ['per', 'int', 'awa'];
      const wounds = this.data.data.health.physical.wounds.value;
      const traumas = this.data.data.health.mental.traumas.value;

      if (
        wounds > 0 &&
        physicalTests.includes(CONFIG.expertiseAttributeMap[skill])
      ) {
        difficulty += wounds;
        tooltip.push({name: 'Wounds', value: wounds});
      }
      if (
        traumas > 0 &&
        mentalTests.includes(CONFIG.expertiseAttributeMap[skill])
      ) {
        difficulty += traumas;
        tooltip.push({name: 'Traumas', value: traumas});
      }
    }

    difficulty += this.data.data.difficultyModifier || 0;

    if (difficulty > 5) difficulty = 5;

    let tooltipText = '';
    if (tooltip.length)
      tooltipText = tooltip.map(i => `${i.name}: +${i.value}`).join('\n');

    return {difficulty, tooltipText};
  }

  _attributeBonus(attribute) {
    const attributeValue = this.data.data.attributes[attribute].value;
    let bonus;
    if (attributeValue <= 8) bonus = 0;
    else if (attributeValue === 9) bonus = 1;
    else if (attributeValue <= 11) bonus = 2;
    else if (attributeValue <= 13) bonus = 3;
    else if (attributeValue <= 15) bonus = 4;
    else bonus = 5;
    return bonus;
  }

  async getRollOptions(rollNames) {
    const flag = this.getFlag(game.system.id, 'rollOptions') ?? {};
    return rollNames
      .flatMap(rollName =>
        // convert flag object to array containing the names of all fields with a truthy value
        Object.entries(flag[rollName] ?? {}).reduce(
          (opts, [key, value]) => opts.concat(value ? key : []),
          []
        )
      )
      .reduce((unique, option) => {
        // ensure option entries are unique
        return unique.includes(option) ? unique : unique.concat(option);
      }, []);
  }

  async addCondition(effect, value = 1) {
    if (typeof effect === 'string')
      effect = duplicate(
        game.conan2d20.config.statusEffects.find(e => e.id === effect)
      );
    if (!effect) return 'No Effect Found';

    if (!effect.id) return 'Conditions require an id field';

    effect.label = game.i18n.localize(effect.label);

    // eslint-disable-next-line prefer-const
    let existing = this.hasCondition(effect.id);
    if (existing && existing.data.flags.conan2d20.value === null) {
      return existing;
    }

    if (existing) {
      existing = duplicate(existing);
      existing.flags.conan2d20.value += value;
      return this.updateEmbeddedDocuments('ActiveEffect', [existing]);
    }

    if (!existing) {
      if (Number.isNumeric(effect.flags.conan2d20.value)) {
        effect.flags.conan2d20.value = value;
      }

      effect['flags.core.statusId'] = effect.id;
    }
    return this.createEmbeddedDocuments('ActiveEffect', [effect]);
  }

  async removeCondition(effect, value = 1) {
    effect = duplicate(
      game.conan2d20.config.statusEffects.find(e => e.id === effect)
    );
    if (!effect) {
      return 'No Effect Found';
    }
    if (!effect.id) {
      return 'Conditions require an id field';
    }

    // eslint-disable-next-line prefer-const
    let existing = this.hasCondition(effect.id);
    if (existing) {
      const duplicated = duplicate(existing);
      duplicated.flags.conan2d20.value -= value;

      if (duplicated.flags.conan2d20.value <= 0) {
        return this.deleteEmbeddedDocuments('ActiveEffect', [existing.id]);
      }

      return this.updateEmbeddedDocuments('ActiveEffect', [duplicated]);
    }
  }

  hasCondition(conditionKey) {
    const existing = this.data.effects.find(
      i => i.data.flags.core.statusId === conditionKey
    );
    return existing;
  }

  // Return the type of the current actor
  get actorType() {
    return this.data.type;
  }
}
